$(document).ready(function() {

	$('.advanced-search__text').click(function(event) {
		$('.advanced-search').toggleClass('advanced-search--active');
		$('.advanced-search__text-wr').toggleClass('advanced-search__text--hide');
		$('.search-list-wr').toggleClass('search-list-wr--active');
	});
	$('.advanced-search__option').click(function(event) {
		$(this).toggleClass('advanced-search__option--active');
	});
	$('.search-list__item').click(function(event) {
		$('.search-list__item').removeClass('search-list__item--cur');
		$(this).toggleClass('search-list__item--cur');
		$('.cur-city').html($(this).text());
		$('.cur-city').addClass('cur-city--selected');
		$('.edit-btn').addClass('cur-city--selected');
		$('.search-term').addClass('search-term--checked');
	});
	$('.edit-btn').click(function(event) {
		$('.search-list__item').removeClass('search-list__item--cur');
		$('.cur-city').html('Регион или город');
		$('.cur-city').removeClass('cur-city--selected');
		$('.edit-btn').removeClass('cur-city--selected');
		$('.search-term').removeClass('search-term--checked');
	});
$(".range").slider({
    min: 0,
    max: 5000,
    values: [2000, 3000],
    range: true,
    animate: "fast",
    slide : function(event, ui) {    
        $(".price-filter__min").val(ui.values[ 0 ]);   
        $(".price-filter__max").val(ui.values[ 1 ]);  
    }    
});
$(".price-filter__min").val($(".range").slider("values", 0));
$(".price-filter__max").val($(".range").slider("values", 1));
$(".range-container input").change(function() {
    var input_left = $(".price-filter__min").val().replace(/[^0-9]/g, ''),    
    opt_left = $(".range").slider("option", "min"),
    where_right = $(".range").slider("values", 1),
    input_right = $(".price-filter__max").val().replace(/[^0-9]/g, ''),    
    opt_right = $(".range").slider("option", "max"),
    where_left = $(".range").slider("values", 0); 
    if (input_left > where_right) { 
        input_left = where_right; 
    }
    if (input_left < opt_left) {
        input_left = opt_left; 
    }
    if (input_left == "") {
    input_left = 0;    
    }        
    if (input_right < where_left) { 
        input_right = where_left; 
    }
    if (input_right > opt_right) {
        input_right = opt_right; 
    }
    if (input_right == "") {
    input_right = 0;    
    }    
    $(".price-filter__min").val(input_left); 
    $(".price-filter__max").val(input_right); 
    if (input_left != where_left) {
        $(".range").slider("values", 0, input_left);
    }
    if (input_right != where_right) {
        $(".range").slider("values", 1, input_right);
    }
});

});